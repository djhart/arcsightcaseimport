"use strict";

process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

const _ = require('lodash');
//const moment = require('moment');
const request = require('request');
//const fs = require('fs');
//const util = require('util');

let reportName;

let config = {
    "debug"            : false,
    "debugKeys"        : false,
    "showCount"        : false,
    "setHeaderType"    : true,
    "printEventHeader" : true,
    "printAttachHeader": true,
    "restURL"           : "https://sol-saturn:8443/www/manager-service/rest",
    "coreURL"           : "https://sol-saturn:8443/www/core-service/rest",
    "baseURL"           : "https://sol-saturn:8443",
    "arcUser"          : encodeURIComponent("edgeCore"),
    "arcPass"          : encodeURIComponent("corvidae")
    /*
     "restURL"           : "https://va-css-arcesm03.intelgd.com:8443/www/manager-service/rest",
     "coreURL"           : "https://va-css-arcesm03.intelgd.com:8443/www/core-service/rest",
     "baseURL"           : "https://va-css-arcesm03.intelgd.com:8443",
     "arcUser"          : "as-edge-svc-acct",
     "arcPass"          : "rW5gaO9$S[._gdCyber"
     */
};


function init() {

    // reportName = process.argv[3].replace(/\'/g, '');
    reportName = 'AlertsPriorityTrend';
    getToken(config.arcUser, config.arcPass, myCallback);
}

let donCall = function (authToken, myIds) {//TODO rename? is this needed?
     getReportIdFromName(authToken, myIds, processReport);
};

let myCallback = (data) => {
    getReportIds(data, donCall);
};


let processReport = ( authToken, reportId ) => {
    runReport(authToken, reportId, getAndPrint);
};

let getAndPrint = ( id ) => {
    getReportData(id);
};

function getReportIds(authToken, callback) {

    let options = {
        url : config.restURL + '/ReportService/findAllIds?authToken=' + authToken + '&alt=json',
        json: true
    };

    request.get(options, (error, response, body) => {
        if (!error && response.statusCode === 200) {
            const idList = body["rep.findAllIdsResponse"]["rep.return"];
            callback(authToken, idList);
        }
    });
}

function getReportIdFromName(authToken, idList, callback) {

    idList.forEach((id) => {
        let options = {
            url   : config.restURL
            + '/ReportService/findByUUID?authToken='
            + authToken + '&id='
            + encodeURIComponent(id)
            + '&alt=json'
            , json: true
        };

        request.get(options, function (error, response, body) {

            if (!error && response.statusCode == 200) {

                let myBody = body["rep.findByUUIDResponse"]["rep.return"];
                if ( myBody.name === reportName ){
                    callback( authToken, myBody.resourceid)
                }
            }
        });

    });
}

function getReportData(id) {
    let options = {
        url : config.baseURL + '/www/manager-service/fileservlet?file.command=download&file.id=' + encodeURIComponent(id)
    };

    request.get(options, (error, response, body) => {
        if (!error && response.statusCode === 200) {
            console.log(body);
        }
    });
}

function runReport(authToken, reportId, callback) {

    let options = {
        url : config.restURL +
        '/ArchiveReportService/initDefaultArchiveReportDownloadById?authToken=' + authToken + '&reportId=' + encodeURIComponent(reportId) + '&reportType=Manual',
        json: true
    };

    request.get(options, (error, response, body) => {

        if (!error && response.statusCode === 200) {
            const archiveId = body['arc.initDefaultArchiveReportDownloadByIdResponse']["arc.return"];
            callback(archiveId);
        }
    });
}

let getToken = (user, pass, callback) => {
    let options = {
        url : config.coreURL + '/LoginService/login?login=' + user + '&password=' + pass,
        json: true
    };

    request.get(options, function (error, response, body) {
        if (!error && response.statusCode === 200) {
            let myBody = encodeURIComponent(body["log.loginResponse"]["log.return"]);
            callback(myBody);
        }
    });
};


init();




