"use strict";

process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

const _ = require('lodash');
const moment = require('moment');
const jp = require('jsonpath');
const request = require('request');

let GLOBAL = require('./config').getConfig();

let mode;
const debug = false;
const delimiter = '|';
const myFiller = '';
let reportName;


function init() {

    let arg = process.argv[2];
    const modes = ['events', 'details', 'attachments', 'stage', 'owners', 'notes', 'report', 'groups'];
    if (_.includes(modes, arg)) {
        mode = arg;
        //console.log("mode:", mode);

        if ( mode === 'report'){
            reportName = process.argv[3].replace(/\'/g, '');
            //console.log("reportName:", reportName);
        }

        getToken(GLOBAL.arcUser, GLOBAL.arcPass, mode, myCallback);
    }
}

let donCall = function (myIds, authToken) {//TODO rename? is this needed?

    if (mode === 'stage') {
        getCaseDetails(myIds, authToken, mode, stageTallies);
    } else if (mode === 'owners') {
        getOwnerIds(myIds, authToken, mode, getOwnerDetails);
    } else if (mode === 'notes') {
        processCaseIds(authToken, myIds);
    } else if (mode === 'groups') {
        getGroupIds(authToken, getGroupDetails);
    } else if (mode === 'report') {
       // getReportIds(myIds, authToken);
    } else {
        getCaseDetails(myIds, authToken, mode);
    }

};

let myCallback = (data, mode) => {
    if (debug) {
        console.log("my token:", data, "\nmode", mode);
    }
    if ( mode === 'report' ){
        getReportIds(data, idFromName);
    } else {
        getCaseIds(data, donCall);
    }
};

let idFromName = ( authToken, idList) => {
    getReportIdFromName( authToken, idList, processReport);
};

let processReport = ( authToken, reportId ) => {
    //console.log("inside processReport: reportId:", reportId);
    runReport(authToken, reportId, getAndPrint);
};

let getAndPrint = ( id ) => {
    //console.log("inside getAndPrint: id:", id);
    getReportData(id);
};

function getGroupIds(authToken, callback) {

    let options = {
        url : GLOBAL.restURL + '/GroupService/findAllIds?authToken=' + encodeURIComponent(authToken) + '&alt=json',
        json: true
    };

    request.get(options, (error, response, body) => {
        if (!error && response.statusCode == 200) {
            const idList = body["gro.findAllIdsResponse"]["gro.return"];

            callback(authToken, idList);
        }
    });
}

function getGroupDetails(authToken, idList, callback) {

    printHeaders(GLOBAL.groupHeaders);

    idList.forEach((id) => {
        let options = {
            url   : GLOBAL.restURL + '/GroupService/findByUUID?authToken=' + authToken + '&id=' + encodeURIComponent(id) + '&alt=json'
            , json: true
        };
        if (debug) {
            console.log("\ndetails url", options.url);
        }

        request.get(options, function (error, response, body) {

            if (!error && response.statusCode == 200) {

                let group = body["gro.findByUUIDResponse"]["gro.return"];

                if (( group.typeName === 'Group [Case]') && (group['URI'].includes('MSSP'))){

                    let values = [
                        group.typeName,
                        group.URI,
                        group.subGroupCount,
                        group.name,
                        group.resourceid
                    ];
                    let entry = _.join(values, '|');
                    console.log(entry);
                }
            }
        });

    });
}

function getReportIds(authToken, callback) {

    let options = {
        url : GLOBAL.restURL + '/ReportService/findAllIds?authToken=' + encodeURIComponent(authToken) + '&alt=json',
        json: true
    };

    request.get(options, (error, response, body) => {

        if (!error && response.statusCode == 200) {
            const idList = body["rep.findAllIdsResponse"]["rep.return"];
            callback(authToken, idList);
        }
    });
}

function getReportIdFromName(authToken, idList, callback) {

    idList.forEach((id) => {
        let options = {
            url   : GLOBAL.restURL + '/ReportService/findByUUID?authToken=' + authToken + '&id=' + encodeURIComponent(id) + '&alt=json'
            , json: true
        };
        if (debug) {
            console.log("\ndetails url", options.url);
        }

        request.get(options, function (error, response, body) {

            if (!error && response.statusCode == 200) {

                let myBody = body["rep.findByUUIDResponse"]["rep.return"];
                if ( myBody.name === reportName ){
                    callback( authToken, myBody.resourceid, getReportData)
                }
            }
        });

    });
}

function getReportData(id) {
    let options = {
        url : GLOBAL.baseURL + '/www/manager-service/fileservlet?file.command=download&file.id=' + encodeURIComponent(id)
    };

    request.get(options, (error, response, body) => {

        if (!error && response.statusCode == 200) {
            console.log(body);
        }
    });
}

function runReport(authToken, reportId, callback) {

    let options = {
        url : GLOBAL.restURL +
        '/ArchiveReportService/initDefaultArchiveReportDownloadById?authToken=' + encodeURIComponent(authToken) + '&reportId=' + encodeURIComponent(reportId) + '&reportType=Manual',
        json: true
    };

    request.get(options, (error, response, body) => {

        if (!error && response.statusCode == 200) {
            const archiveId = body['arc.initDefaultArchiveReportDownloadByIdResponse']["arc.return"];
            callback(archiveId);
        }
    });
}


function getReportNameFromId(id, authToken, callback) {

        let options = {
            url   : GLOBAL.restURL + '/ReportService/findByUUID?authToken=' + authToken + '&id=' + encodeURIComponent(id) + '&alt=json'
            , json: true
        };
        if (debug) {
            console.log("\ndetails url", options.url);
        }

        request.get(options, function (error, response, body) {
            // console.log("query body", body);

            if (!error && response.statusCode == 200) {

                let myBody = body["rep.findByUUIDResponse"]["rep.return"];
                console.log("\nname:", myBody.name);
                console.log("resourceid:", myBody.resourceid);
            }
        });
}


function getData() {
    return GLOBAL.myData;
}

function clearData() {
    GLOBAL.myData = [];
}

function getOwnerDetails(authToken, owners) {
    console.log("in getOwnerDetails and owners:", owners.length, owners);
    clearData();
    let size = owners.length;


    _.each(owners, (ownerPair) => {

        let pair = _.split(ownerPair, '|');
        let ownerId = pair[1];
        let caseId = pair[0];
        let entry = {};

        let myJson = {
            "res.getResourceById": {
                "res.authToken" : authToken,
                "res.resourceId": encodeURI(ownerId),
            }
        };

        console.log("owners:", myJson);


        let requestObject = {

            url    : GLOBAL.restURL + '/ResourceService/getResourceById',
            method : 'POST',
            headers: [{name: 'Content-Type', value: 'application/json'}],
            json   : myJson
        };

        request.post(requestObject, (error, response, body) => {

            if (!error && response.statusCode == 200) {
                let myBody = body["res.getResourceByIdResponse"]["res.return"];
                entry[caseId] = myBody.name;
                addEntry(entry);

            }
            size--;
            if (size < 1) {
                console.log("caseId|owners");

                let jd = getData();


                _.each(jd, (bark) => {
                    let lilly = _.keys(bark)[0];
                    let ruth = _.map(jd, _.keys(bark)[0]);
                    _.pull(ruth, undefined);
                    console.log(lilly + delimiter + _.join(ruth, ','));
                });


                //callback(authToken, getData()); //TODO uncomment!
            }
        });

    })
}
function getOwnerIds(idList, authToken, mode, callback) {
    if (debug) {
        console.log("In getOwnerIds and mode:", mode);
    }
    let size = idList.length;

    //let myType = 13; //parent
    let myType = 8; //Note

    idList.forEach((id) => {
        let options = {
            url   : GLOBAL.restURL + '/CaseService/getRelationshipsOfThisAndParents?relationshipType=' + myType + '&authToken=' +
            encodeURIComponent(authToken) +
            '&sourceId=' +
            encodeURI(id) +
            '&alt=json'
            , json: true
        };
        if (debug) {
            console.log("\nOwnerIds url", options.url);
        }

        request.get(options, function (error, response, body) {
            if (!error && response.statusCode == 200) {
                let myBody = body["cas.getRelationshipsOfThisAndParentsResponse"]["cas.return"];

                if (debug) {
                    console.log("myBody:", myBody);
                }

                if (typeof myBody === 'string') {
                    addEntry(id + delimiter + myBody);
                } else if (typeof myBody === 'object') {
                    _.each(myBody, function (b) {
                        addEntry(id + delimiter + b);
                    });
                } else {
                    //do nothing
                }
            }
            size--;

            if (size < 1) {
                callback(authToken, getData());
            }
        });
    });
}

function getCaseDetails(idList, authToken, mode, callback) {
    let printHeader = true;
    let numIds = idList.length;
    let donsData = [];

    idList.forEach((id) => {
        let options = {
            url   : GLOBAL.restURL + '/CaseService/findByUUID?authToken=' + authToken + '&id=' + encodeURIComponent(id) + '&alt=json'
            , json: true
        };
        if (debug) {
            console.log("\ndetails url", options.url);
        }

       request.get(options, function (error, response, body) {

            if (!error && response.statusCode == 200) {

                let myBody = body["cas.findByUUIDResponse"]["cas.return"];

               // console.warn("current myBody =", myBody);
                if ((myBody.stage !== 'INITIAL') && (myBody['URI'].includes('MSSP'))){




                        if (mode === 'details') {
                        if (printHeader) {
                            printHeaders(GLOBAL.caseKeys);
                            printHeader = false;
                        }
                        sendText(myBody, GLOBAL.caseKeys);
                    }

                    if (mode === 'stage') { //TODO these will include ids that haven't been filtered
                        donsData.push(myBody['stage']);

                        if (numIds <= 1) {
                            //TODO put scrub code here to select specific ids

                            callback(donsData);
                        }
                    }

                    if (mode === 'events') {
                        if (typeof myBody['eventIDs'] === 'undefined') {
                            myBody['eventIDs'] = '';
                        }
                        getEventDetails(myBody['eventIDs'], authToken, id);
                    }

                    if (mode === 'attachments') {
                        if (typeof myBody['attachmentIDs'] === 'object') {
                            getAttachmentDetails(myBody['attachmentIDs'], authToken, id);
                        }
                    }
                }
            }
            numIds--;
        });

    });

}

function flattenObject(source, keys) {
    let dataObject = JSON.parse(source);

    _.each(keys, function (f) {
        let result = jp.query(dataObject, '$..' + f);

        let field = (typeof result[0] === 'undefined') ? myFiller : result[0];

        let formatted = _.replace(field.toString(), /["|,]+/g, '');

        // replace some of arcsights default values with spaces
        if (_.includes(GLOBAL.badDefaults, formatted)) {
            formatted = '';
        }
        addEntry(formatted + delimiter);
    });

}

function sendText(o, keys, caseId, callback) {
    let skip = false;
    let customer = '';


    /*
     This is needed to keep the tabular columns aligned
     caseId is in th keys array so that it gets printed in the
     headers so we write the value of caseId here and remove the key
     since it's not actually a value in the incoming object
     */
    if (caseId) {
        addEntry(caseId + delimiter);
        _.pull(keys, 'caseId');
    }

    /*
     Now that the original key list was used or headers, we'll pull out the src_ fields since they will be
     expanded from the 'source' field.
     */
    if (mode === 'events') {
        keys = _.xor(keys, GLOBAL.toPull);
    }

    //THis must come after the two key _pulls above
    let size = keys.length;

    _.each(keys, (key) => {
        skip = false;

        let field = o[key];

        if ( key === 'URI' ){
            if ( field.includes('MSSP/Open')) {
                let words = _.words(field);
                customer = words[6];
            }
        }

        if ( key === 'customer'){
            field = customer;
        }

        if (typeof field === 'undefined') {
            field = '';
        }

        // process the source object and print the contained fields.
        // Skip printing the actual source field.
        if (( _.includes(GLOBAL.objectsToFlatten, key)) && ( mode === 'events' )) {

            let choices = {
                source  : GLOBAL.src_fields,
                customer: GLOBAL.customer_fields,
                category: GLOBAL.category_fields
            };

            if (typeof field !== 'object') {
                _.each(choices[key], (filler) => {
                    if (GLOBAL.debugKeys) {
                        myFiller = filler;
                    }
                    addEntry(myFiller + delimiter);
                })
            } else {
                flattenObject(JSON.stringify(field), choices[key]);
            }

            // We expanded the 'source' field object and wrote those results
            skip = true;
        }
        //convert objects to dates where needed
        if (_.includes(GLOBAL.timeObjects, key)) {
            if (typeof field === 'object') {
                field = parseTime(field);
            }
        }

        if (!skip) {
            /*
             Determine the item type, strip newlines/CRs where needed
             and print using the appropriate method for the type.
             */
            if (( typeof field === 'string') || ( typeof field === 'boolean')) {
                let formatted = _.replace(_.replace(field.toString(), /\r?\n|\r/g, ''), /["|,]+/g, ''); //uh..not pretty
                addEntry(formatted);
            } else {
                addEntry(field.toString());
            }
            addEntry(printLineEnd(size));
            skip = false;
        }
        size--;
    });

    console.log(_.join(GLOBAL.myData, ''));
    clearData();
}

function addEntry(entry) {
    GLOBAL.myData.push(entry);
}

function printLineEnd(size) {
    return (size > 1) ? delimiter : '';
}

let getToken = (user, pass, mode, callback) => {
    let options = {
        url : GLOBAL.coreURL + '/LoginService/login?login=' + encodeURIComponent(user) + '&password=' + encodeURIComponent(pass),
        json: true
    };

    request.get(options, function (error, response, body) {
        if (!error && response.statusCode == 200) {
            let myBody = body["log.loginResponse"]["log.return"];
            callback(myBody, mode);
        }
    });
};

function getCaseIds(authToken, callback) {

    let options = {
        url : GLOBAL.restURL + '/CaseService/findAllIds?authToken=' + encodeURIComponent(authToken) + '&alt=json',
        json: true
    };

    request.get(options, (error, response, body) => {
        if (!error && response.statusCode == 200) {
            const idList = body["cas.findAllIdsResponse"]["cas.return"];

            callback(idList, authToken);
        }
    });
}

function getEventDetails(idList, authToken, caseId) {

    let myJson = {
        "sev.getSecurityEvents": {
            "sev.authToken"  : authToken,
            "sev.ids"        : idList,
            "sev.startMillis": "-1",
            "sev.endMillis"  : "-1"
        }
    };

    let requestObject = {

        url    : GLOBAL.restURL + '/SecurityEventService/getSecurityEvents',
        method : 'POST',
        headers: [{name: 'Content-Type', value: 'application/json'}],
        json   : myJson
    };

    request.post(requestObject, (error, response, body) => {

        if (!error && response.statusCode == 200) {

            // we are expanding source so we do not want to print the header
            let headerKeys = _.xor(GLOBAL.eventKeys, GLOBAL.objectsToFlatten);

            if (GLOBAL.printEventHeader) {
                printHeaders(headerKeys);
                GLOBAL.printEventHeader = false;
            }

            const myBody = body["sev.getSecurityEventsResponse"]["sev.return"];
            _.each(myBody, function (d) {

                if ( typeof d === 'object'){
                    sendText(d, GLOBAL.eventKeys, caseId);
                }
            });
        }
    });
}

function printHeaders(keys) {
    let size = keys.length;

    _.each(keys, (key) => {
        let header;

        if (GLOBAL.setHeaderType) {

            if ((_.includes(GLOBAL.timeObjects, key)) || (_.includes(GLOBAL.timestampList, key))) {
                header = key.concat('*DATE*millis');
            }
            else if (_.includes(GLOBAL.booleanList, key)) {
                header = key.concat('*BOOLEAN');
            }
            else if (_.includes(GLOBAL.intList, key)) {
                header = key.concat('*INT');
            }
            else if (_.includes(GLOBAL.numberList, key)) {
                header = key.concat('*NUMBER');
            } else {
                header = key;
            }
        } else {
            header = key;
        }
        addEntry(header);
        addEntry(printLineEnd(size));
        size--;
    });
    if (GLOBAL.showCount) {
        console.log(GLOBAL.myData.length.toString(), _.join(GLOBAL.myData, ''));
    } else {
        console.log(_.join(GLOBAL.myData, ''));
    }
    GLOBAL.myData = [];
}

function getAttachmentDetails(idList, authToken, caseId) {

    _.each(idList, function (id) {

        let myJson = {
            "res.getResourceById": {
                "res.authToken" : authToken,
                "res.resourceId": encodeURI(id),

            }
        };

        let requestObject = {

            url    : GLOBAL.restURL + '/ResourceService/getResourceById',
            method : 'POST',
            headers: [{name: 'Content-Type', value: 'application/json'}],
            json   : myJson
        };
        request.post(requestObject, function (error, response, body) {

            if (!error && response.statusCode == 200) {

                if (GLOBAL.printAttachHeader) {
                    printHeaders(GLOBAL.attachKeys);
                    GLOBAL.printAttachHeader = false;
                }

                if (debug) {
                    console.log("\nin attachments:", body);
                }

                const myBody = body["res.getResourceByIdResponse"]["res.return"];
                sendText(myBody, GLOBAL.attachKeys, caseId);
            }
        });
    });
}

function stageTallies(stageDetails) {
    printHeaders(GLOBAL.stageHeaders);

    let tally = {
        TOTAL    : stageDetails.length,
        QUEUED   : 0,
        INITIAL  : 0,
        FOLLOW_UP: 0,
        FINAL    : 0,
        CLOSED   : 0
    };
    _.each(stageDetails, (stage) => {
        tally[stage]++;
    });

    _.each(GLOBAL.stageKeys, (key) => {
        console.log(key + delimiter + tally[key]);
    });
}

function parseTime(o) {
    let m = moment([o.year, o.month, o.day, o.hour, o.minute, o.second, o.milliSecond]);
    return m.valueOf();
}

function getNoteDetails(authToken, caseId, noteIds) {

    _.each(noteIds, function (noteId) {

        let myJson = {
            "res.getResourceById": {
                "res.authToken" : authToken,
                "res.resourceId": noteId
            }
        };

        let requestObject = {

            url    : GLOBAL.restURL + '/ResourceService/getResourceById',
            method : 'POST',
            headers: [{name: 'Content-Type', value: 'application/json'}],
            json   : myJson
        };
        request.post(requestObject, function (error, response, body) {

            if (!error && response.statusCode == 200) {

                let note = body["res.getResourceByIdResponse"]["res.return"];
                //console.log("\nnote body:", note);
                let values = [
                    caseId,
                    note.createdTimestamp,
                    note.creatorName,
                    note.localID,
                    note.modificationCount,
                    note.modifiedTimestamp,
                    note.modifierName,
                    note.name,
                    note.resourceid,
                    note.state,
                    note.type,
                    note.typeName,
                    note.URI
                ];
                let entry = _.join(values, '|');
                console.log(entry);
            }
        })
    })
}

function processCaseIds(authToken, idList) {
    printHeaders(GLOBAL.noteHeaders);

    _.each(idList, function (id) {
        getNoteIdsForCaseId(authToken, id, getNoteDetails);
    });
}

function getNoteIdsForCaseId(authToken, caseId, callback) {

    let myJson = {
        "cas.getSourcesWithThisTargetByRelationshipForResourceId": {
            "cas.authToken"       : authToken,
            "cas.sourceResourceId": caseId,
            "cas.relationshipType": 5
        }
    };

    let requestObject = {

        url    : GLOBAL.restURL + '/CaseService/getSourcesWithThisTargetByRelationshipForResourceId',
        method : 'POST',
        headers: [{name: 'Content-Type', value: 'application/json'}],
        json   : myJson
    };

    request.post(requestObject, function (error, response, body) {
        if (!error && response.statusCode == 200) {
            let myBody = body["cas.getSourcesWithThisTargetByRelationshipForResourceIdResponse"]["cas.return"];
            if (typeof myBody !== 'undefined') {
                callback(authToken, caseId, myBody);
            }
        }
    });
}


init();




