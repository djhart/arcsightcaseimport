"use strict";

process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

let loki = require('lokijs');

//TODO need to scrub case ids and only process interesting: MSP, not inital

const _ = require('lodash');
const moment = require('moment');
const jp = require('jsonpath');
const request = require('request');

let GLOBAL = require('./config').getConfig();

let mode = 'not_set';
const debug = false;
//let myData = [];
const delimiter = '|';
let myFiller = '';
let caseIds;
let caseDetails;
let attachmentIds;
let eventIds;

function init() {

    let addCollectionOptions = {
        //unique: ['id'],
        ttlInterval: 1200,
        autoupdate: true
    };

    caseIds = db.getCollection('caseIds');

    if ( !caseIds ){
        caseIds = db.addCollection('caseIds', addCollectionOptions);
    }

    caseDetails = db.getCollection('caseDetails');
    if ( !caseDetails ){
        caseDetails = db.addCollection('caseDetails', addCollectionOptions);
    }

    eventIds = db.getCollection('eventIds');
    if ( !eventIds ){
        eventIds = db.addCollection('eventIds', addCollectionOptions);
    }

    attachmentIds = db.getCollection('attachmentIds');
    if ( !attachmentIds ){
        attachmentIds = db.addCollection('attachmentIds', addCollectionOptions);
    }

    console.log("listening for events");
    caseDetails.on('loaded', function(changeDoc){
        console.warn("\n\n\n\n CHANGED IN CaseIds \n\n");
    });

    attachmentIds.on('loaded', function(changeDoc){
        console.warn("\n\n\n\n CHANGED IN attachmentIds \n\n");
    });


    let mode = 'details';
    getToken(GLOBAL.arcUser, GLOBAL.arcPass, mode, myCallback);


}

let donCall = function (authToken) {//TODO rename? is this needed?

        getCaseDetails(myIds, authToken, mode);
};


function getCaseDetails(authToken, mode, callback) {
    let idList = caseIds.get(1);
    let ids = idList['myList'];//TODO unique this just in case there are dupes
    let numIds = ids.length;

    ids.forEach((id) => {
        let hasEvents = false;
        let hasAttachments = false;
        let skip = false;
        let options = {
            url   : GLOBAL.arcURL + '/www/manager-service/rest/CaseService/findByUUID?authToken=' + authToken + '&id=' + encodeURIComponent(id) + '&alt=json'
            , json: true
        };
        //console.log("url", options.url);
        request.get(options, function (error, response, body) {
            //console.log("body", body);
            if (!error && response.statusCode == 200) {

                let myBody = body["cas.findByUUIDResponse"]["cas.return"];


                //console.log("Stage", myBody.stage);

                if ( myBody.stage === 'INITIAL'){
                    skip = true;

                    console.log("URI", myBody.URI);

                    if ( myBody.eventIDs ){
                        console.log("has events", JSON.stringify(myBody.eventIDs));
                        hasEvents = true;
                    }
                    if ( myBody.attachmentIDs ){
                        console.log("has attachments", JSON.stringify(myBody.attachmentIDs));
                        let myO = { id: id,
                            attachmentIDs: JSON.stringify(myBody.attachmentIDs)
                        };
                        //console.warn("myO", myO);
                        attachmentIds.insert(myO);
                        db.saveDatabase();
                        hasAttachments = true;
                    }
                }

                if (!(skip) && ( numIds > 1 )){
                    sendText(myBody, GLOBAL.caseKeys, myBody.resourceid, hasEvents, hasAttachments);
                } else {
                    sendText(myBody, GLOBAL.caseKeys, myBody.resourceid, hasEvents, hasAttachments, saveAll);

                }

            }
            numIds--;

        });
    });
}

let myCallback = (data, mode) => {
    console.warn("in myCallback and data is ", data);
    getCaseIds(data, saveCallback);
};

let saveCallback = ( data, mode) => {
    getCaseDetails(data,mode, saveAll);
};

function getCaseIds(authToken, callback) {

    let options = { url: GLOBAL.arcURL + '/www/manager-service/rest/CaseService/findAllIds?authToken=' + encodeURIComponent(authToken) + '&alt=json', json: true};

    request.get(options, (error, response, body) => {
        let ids;
        if (!error && response.statusCode == 200) {
            let idList = [];
            ids = jp.query(body, '$..*');
            ids[1].forEach(function (id) {
                    idList.push(id.toString());
                }
            );
            caseIds.insert({ myList:idList});
            db.saveDatabase();

            callback(authToken);
        }
    });
}

function printHeaders(keys) {
    let size = keys.length;

    _.each(keys, (key) => {
        let header;

        if (GLOBAL.setHeaderType) {

            if ((_.includes(GLOBAL.timeObjects, key)) || (_.includes(GLOBAL.timestampList, key))) {
                header = key.concat('*DATE*millis');
            }
            else if (_.includes(GLOBAL.booleanList, key)) {
                header = key.concat('*BOOLEAN');
            }
            else if (_.includes(GLOBAL.intList, key)) {
                header = key.concat('*INT');
            }
            else if (_.includes(GLOBAL.numberList, key)) {
                header = key.concat('*NUMBER');
            } else {
                header = key;
            }
        } else {
            header = key;
        }
        addEntry(header);
        addEntry(printLineEnd(size));
        size--;
    });
    if (GLOBAL.showCount) {
        console.log(GLOBAL.myData.length.toString(), _.join(GLOBAL.myData, ''));
    } else {
        console.log(_.join(GLOBAL.myData, ''));
    }
    GLOBAL.myData = [];
}

function flattenObject(source, keys) {
    let dataObject = JSON.parse(source);

    _.each(keys, function (f) {
        let result = jp.query(dataObject, '$..' + f);

        let field = (typeof result[0] === 'undefined') ? myFiller : result[0];

        let formatted = _.replace(field.toString(), /["|,]+/g, '');

        // replace some of arcsights default values with spaces
        if (_.includes(GLOBAL.badDefaults, formatted)) {
            formatted = '';
        }

        if (GLOBAL.debugKeys) {
            formatted = f;
        }
        addEntry(formatted + delimiter);
    });

}


function sendText(o, keys, id, hasEvents, hasAttachments, callback) {
    //console.warn("in sendText id is ", id);
    let entry = [];
    let size = keys.length;

    _.each(keys, (key) => {
        let field = o[key];

        if (typeof field === 'undefined') {
            field = '';
        }
        //convert objects to dates where needed
        if (_.includes(GLOBAL.timeObjects, key)) {
            if (typeof field === 'object') {
                field = parseTime(field);
            }
        }

        if (( typeof field === 'string') || ( typeof field === 'boolean')) {
            let formatted = _.replace(_.replace(field.toString(), /\r?\n|\r/g, ''), /["|,]+/g, ''); //uh..not pretty
            entry.push(formatted);
        } else {
            entry.push(field.toString());
        }
        entry.push(printLineEnd(size));
        size--;
    });
    let myO = {};
    let final = _.join(entry, '');
    myO= { id: id,
        hasEvents: hasEvents,
        hasAttachments: hasAttachments,
        final: final

    };
    //console.warn("myO", myO);
    caseDetails.insert(myO);
    db.saveDatabase();
    //console.log("bottom sendText id", id);
    //console.log("\n\nentry", final);
    //clearData();
    if ( callback ){
        callback();

    }
}

function sendText_orig(o, keys, caseId, callback) {
    let skip = false;

    /*
     This is needed to keep the tabular columns aligned
     caseId is in th keys array so that it gets printed in the
     headers so we write the value of caseId here and remove the key
     since it's not actually a value in the incoming object
     */
    if (caseId) {
        addEntry(caseId + delimiter);
        _.pull(keys, 'caseId');
    }

    /*
     Now that the original key list was used or headers, we'll pull out the src_ fields since they will be
     expanded from the 'source' field.
     */
    if (mode === 'events') {
        keys = _.xor(keys, GLOBAL.toPull);
    }

    //THis must come after the two key _pulls above
    let size = keys.length;

    _.each(keys, (key) => {
        skip = false;

        let field = o[key];

        if (typeof field === 'undefined') {
            field = '';
        }

        // process the source object and print the contained fields.
        // Skip printing the actual source field.
        if (( _.includes(GLOBAL.objectsToFlatten, key)) && ( mode === 'events' )) {

            let choices = {
                source  : GLOBAL.src_fields,
                customer: GLOBAL.customer_fields,
                category: GLOBAL.category_fields
            };

            if (typeof field !== 'object') {
                _.each(choices[key], (filler) => {
                    if (GLOBAL.debugKeys) {
                        myFiller = filler;
                    }
                    addEntry(myFiller + delimiter);
                })
            } else {
                flattenObject(JSON.stringify(field), choices[key]);
            }

            // We expanded the 'source' field object and wrote those results
            skip = true;
        }
        //convert objects to dates where needed
        if (_.includes(GLOBAL.timeObjects, key)) {
            if (typeof field === 'object') {
                field = parseTime(field);
                if (GLOBAL.debugKeys) {
                    field = key;
                }
            }
        }

        if (!skip) {
            /*
             Determine the item type, strip newlines/CRs where needed
             and print using the appropriate method for the type.
             */
            if (GLOBAL.debugKeys) {
                field = key;
            }
            if (( typeof field === 'string') || ( typeof field === 'boolean')) {
                let formatted = _.replace(_.replace(field.toString(), /\r?\n|\r/g, ''), /["|,]+/g, ''); //uh..not pretty
                addEntry(formatted);
            } else {
                addEntry(field.toString());
            }
            addEntry(printLineEnd(size));
            skip = false;
        }
        size--;
    });
    if (GLOBAL.showCount) {
        console.warn("myData:", GLOBAL.myData.length, _.join(GLOBAL.myData, ''));
    } else {
        console.log(_.join(GLOBAL.myData, ''));
    }
    clearData();
    //callback(); //TODO use this for persist later
}

function hello(){
    console.log("hello!");
}

function getData(){
    return GLOBAL.myData;
}

function clearData(){
    GLOBAL.myData = [];
}


function addEntry(entry) {
    GLOBAL.myData.push(entry);
}

function printLineEnd(size) {
    return (size > 1) ? delimiter : '';
}

let getToken = (user, pass, mode, callback) => {
    let options = {
        url : GLOBAL.arcURL + '/www/core-service/rest/LoginService/login?login=' + user + '&password=' + pass,
        json: true
    };

    request.get(options, function (error, response, body) {
        if (!error && response.statusCode == 200) {
            callback(jp.query(body, '$..*')[1], mode);
        }
    });
};



function parseTime(o) {
    let m = moment([o.year, o.month, o.day, o.hour, o.minute, o.second, o.milliSecond]); // TODO leave milliSecond spelled this way for now
    return m.valueOf();
}

// Code to use later with persist

let db = new loki('db_name.json', {
    persistenceMethod:'fs',
    autosave: true,
    autosaveInterval: 60 * 1000, //every 60 seconds
    autoload: true,
    autoloadCallback: init
});

function saveAll(){
    db.saveDatabase();
    }

function saveAndExit(){
    db.saveDatabase();
    _.delay(function(donsEnd){
        console.warn("\n\n\nWAITING for a few seconds.......\n");
    }),20000 };

function donsEnd(){
    console.log("ending.....");
    process.exit(0);
}


//init();




