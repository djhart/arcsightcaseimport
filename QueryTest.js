"use strict";

process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

//TODO need to scrub case ids and only process interesting: MSP, not inital

const _ = require('lodash');
const moment = require('moment');
const jp = require('jsonpath');
const request = require('request');

let GLOBAL = require('./config').getConfig();


let mode;
const debug = false;


function init() {
    console.log("starting and", GLOBAL.restURL);
    getToken(GLOBAL.arcUser, GLOBAL.arcPass, mode, myCallback);

}

let donCall = function (authToken, myIds) {//TODO rename? is this needed?
        getGroupDetails(authToken, myIds);

};

function getGroupIds(authToken, callback) {

    let options = {
        url : GLOBAL.restURL + '/GroupService/findAllIds?authToken=' + encodeURIComponent(authToken) + '&alt=json',
        json: true
    };

    request.get(options, (error, response, body) => {
        console.log("body", body);
        if (!error && response.statusCode == 200) {
            const idList = body["gro.findAllIdsResponse"]["gro.return"];

            callback(authToken, idList);
        }
    });
}

function getGroupDetails(authToken, idList, callback) {
    console.log("in getGroupDetails\n");

    //printHeaders(GLOBAL.groupHeaders);

    idList.forEach((id) => {
        let options = {
            url   : GLOBAL.restURL + '/GroupService/findByUUID?authToken=' + authToken + '&id=' + encodeURIComponent(id) + '&alt=json'
            , json: true
        };
        if (debug) {
            console.log("\ndetails url", options.url);
        }

        request.get(options, function (error, response, body) {

            if (!error && response.statusCode == 200) {

                let group = body["gro.findByUUIDResponse"]["gro.return"];

                if (( group.typeName === 'Group [Case]') && (group['URI'].includes('MSSP'))){
                    let values = [
                        group.typeName,
                        group.URI,
                        group.subGroupCount,
                        group.name,
                        group.resourceid
                    ];
                    let entry = _.join(values, '|');
                    console.log(entry);
                }
            }
        });

    });
}


let getToken = (user, pass, mode, callback) => {
    let options = {
        url : GLOBAL.coreURL + '/LoginService/login?login=' + user + '&password=' + pass,
        json: true
    };

    request.get(options, function (error, response, body) {
        if (!error && response.statusCode == 200) {
            let myBody = body["log.loginResponse"]["log.return"];
            callback(myBody, mode);
        }
    });
};

let myCallback = (data, mode) => {
    //runReport(data, donCall);
    getGroupIds(data, donCall);
    //getGroupDetails(data);
};

init();




