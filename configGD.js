/**
 * Created by hart on 11/7/16.
 */
"use strict";

exports.getConfig = function getConfig() {


    /*
     The eventKeys list provides header fields as well as a property list used to retrieve values from Arcsight.  This isn't as straight
     forward as I'd like, however; because some fields represent objects which will be expanded into many more fields.  The src_fields variable is an example of
     what will be expanded from the eventKeys->source field.
     */

/*
"TOTAL",
"QUEUED",
"INITIAL",
"FOLLOW_UP",
"FINAL",
"CLOSED"
*/

    let config = {
        "debug"            : false,
        "debugKeys"        : false,
        "showCount"        : false,
        "setHeaderType"    : true,//TODO set to true
        "printEventHeader" : true,
        "printAttachHeader": true,
        /*
        "restURL"           : "https://sol-saturn:8443/www/manager-service/rest",
        "coreURL"           : "https://sol-saturn:8443/www/core-service/rest",
        "baseURL"           : "https://sol-saturn:8443",
        "arcUser"          : "edgeCore",
        "arcPass"          : "corvidae",
        */
        "restURL"           : "https://va-css-arcesm03.intelgd.com:8443/www/manager-service/rest",
        "coreURL"           : "https://va-css-arcesm03.intelgd.com:8443/www/core-service/rest",
        "baseURL"           : "https://va-css-arcesm03.intelgd.com:8443",
         "arcUser"          : "as-edge-svc-acct",
         "arcPass"          : "rW5gaO9$S[._gdCyber",



        "myData"           : [],
        "badDefaults"      : [
            "-9223372036854776000",
            "-2147483648"
        ],
        "intList"          : [
            "agentSeverity",
            "aggregatedEventCount",
            "assetCriticality",
            "baseEventCount",
            "bytesIn",
            "bytesOut",
            "contentLength",
            "correlatedEventCount",
            "displayID",
            "expirationDate",
            "locality",
            "managerId",
            "modelConfidence",
            "modificationCount",
            "numberOfOccurences",
            "priority",
            "relevance",
            "reportingLevel",
            "severity",
            "state",
            "ttl",
            "uploadSourceId"
        ],
        "stageHeaders"          : [
            "Stage",
            "Count"
        ],
        "noteHeaders"          : [
            "caseId",
            "createdTimestamp",
            "creatorName",
            "localID",
            "modificationCount",
            "modifiedTimestamp",
            "modifierName",
            "name",
            "resourceid",
            "state",
            "type",
            "typeName",
            "URI"
        ],
        "groupHeaders"          : [
            "typeName",
            "URI",
            "subGroupCount",
            "name",
            "resourceid"
        ],
        "stageKeys"          : [
            "TOTAL",
            "QUEUED",
            "INITIAL",
            "FOLLOW_UP",
            "FINAL",
            "CLOSED"
        ],
        "booleanList"      : [
            "attributeInitializationInProgress",
            "deprecated",
            "disabled",
            "fileContentsAvaliable",
            "fileValidOnFileSystem",
            "inCache",
            "inactive",
            "initialized",
            "isAdditionalLoaded",
            "isModifiable",
            "storedOnFileSystem",
            "valid"
        ],
        "numberList"       : [
            "localID",
            "eventId"
        ],
        "timestampList"    : [
            "createdTimestamp",
            "endTime",
            "managerReceiptTime",
            "modifiedTimestamp",
            "startTime"
        ],
        "attachKeys"       : [
            "caseId",
            "createdTimestamp",
            "creatorName",
            "deprecated",
            "disabled",
            "inCache",
            "inactive",
            "initialized",
            "isAdditionalLoaded",
            "localID",
            "modificationCount",
            "modifiedTime",
            "modifiedTimestamp",
            "modifierName",
            "name",
            "resourceid",
            "state",
            "type",
            "typeName",
            "URI",
            "contentLength",
            "expirationDate",
            "fileContentsAvaliable",
            "fileName",
            "fileValidOnFileSystem",
            "mimePrimaryType",
            "mimeSecondaryType",
            "mimeType",
            "mimeTypeAttributes",
            "storedOnFileSystem",
            "uploadSourceId",
            "valid"
        ],
        "timeObjects"      : [
            "createdTime",
            "attackTime",
            "detectionTime",
            "estimatedRestoreTime",
            "estimatedStartTime",
            "lastOccurenceTime",
            "modifiedTime"
        ],
        "caseKeys"         : [
            "alias",
            "URI",
            "customer",
            "action",
            "actionsTaken",
            "affectedElements",
            "affectedServices",
            "affectedSites",
            "associatedImpact",
            "attachmentIDs",
            "attackAddress",
            "attackAgent",
            "attackImpact",
            "attackLocationID",
            "attackMechanism",
            "attackNode",
            "attackOS",
            "attackProgram",
            "attackProtocol",
            "attackService",
            "attackTarget",
            "attackTime",
            "attributeInitializationInProgress",
            "consequenceSeverity",
            "createdTime",
            "creatorName",
            "description",
            "detectionTime",
            "displayID",
            "estimatedImpact",
            "estimatedRestoreTime",
            "estimatedStartTime",
            "finalReportAction",
            "followupContact",
            "history",
            "lastOccurenceTime",
            "localID",
            "modificationCount",
            "modifiedTime",
            "modifiedTimestamp",
            "modifierName",
            "name",
            "numberOfOccurences",
            "operationalImpact",
            "resistance",
            "resourceid",
            "securityClassification",
            "securityClassificationCode",
            "sensitivity",
            "sourceAddress",
            "stage",
            "state",
            "ticketType",
            "vulnerability",
            "vulnerabilityData",
            "vulnerabilityEvidence",
            "vulnerabilitySource",
            "vulnerabilityType1",
            "vulnerabilityType2"
        ],
        "toPull"           : [
            "src_address",
            "src_assetLocalId",
            "src_hostName",
            "src_macAddress",
            "src_translatedAddress",
            "src_zone_externalID",
            "src_zone_id",
            "src_zone_managerID",
            "src_zone_referenceID",
            "src_zone_referenceName",
            "src_zone_referenceString",
            "src_zone_referenceType",
            "src_zone_uri",
            "src_geo_countryCode",
            "src_geo_latitude",
            "src_geo_latitudeLong",
            "src_geo_locationInfo",
            "src_geo_longitude",
            "src_geo_longitudeLong",
            "src_geo_postalCode",
            "src_geo_regionCode",
            "src_port",
            "src_processId",
            "src_translatedPort",
            "cat_behavior",
            "cat_deviceGroup",
            "cat_object",
            "cat_outcome",
            "cat_significance",
            "cat_technique",
            "cust_id",
            "cust_managerID",
            "cust_referenceID",
            "cust_referenceName",
            "cust_referenceString",
            "cust_referenceType",
            "cust_uri"
        ],
        "eventKeys"        : [
            "caseId",
            "agentReceiptTime",
            "agentSeverity",
            "aggregatedEventCount",
            "assetCriticality",
            "baseEventCount",
            "category",
            "cat_behavior",
            "cat_deviceGroup",
            "cat_object",
            "cat_outcome",
            "cat_significance",
            "cat_technique",
            "correlatedEventCount",
            "customer",
            "cust_id",
            "cust_managerID",
            "cust_referenceID",
            "cust_referenceName",
            "cust_referenceString",
            "cust_referenceType",
            "cust_uri",
            "endTime",
            "eventId",
            "locality",
            "managerId",
            "managerReceiptTime",
            "modelConfidence",
            "name",
            "originator",
            "priority",
            "relevance",
            "sessionId",
            "severity",
            "source",
            "src_address",
            "src_assetLocalId",
            "src_hostName",
            "src_macAddress",
            "src_translatedAddress",
            "src_zone_externalID",
            "src_zone_id",
            "src_zone_managerID",
            "src_zone_referenceID",
            "src_zone_referenceName",
            "src_zone_referenceString",
            "src_zone_referenceType",
            "src_zone_uri",
            "src_geo_countryCode",
            "src_geo_latitude",
            "src_geo_latitudeLong",
            "src_geo_locationInfo",
            "src_geo_longitude",
            "src_geo_longitudeLong",
            "src_geo_postalCode",
            "src_geo_regionCode",
            "src_port",
            "src_processId",
            "src_translatedPort",
            "startTime",
            "transportProtocol",
            "ttl",
            "type"
        ],
        "src_fields"       : [
            "address",
            "assetLocalId",
            "hostName",
            "macAddress",
            "translatedAddress",
            "externalID",
            "id",
            "managerID",
            "referenceID",
            "referenceName",
            "referenceString",
            "referenceType",
            "uri",
            "countryCode",
            "latitude",
            "latitudeLong",
            "locationInfo",
            "longitude",
            "longitudeLong",
            "postalCode",
            "regionCode",
            "port",
            "processId",
            "translatedPort"
        ],
        "category_fields"  : [
            "behavior",
            "deviceGroup",
            "object",
            "outcome",
            "significance",
            "technique"
        ],
        "customer_fields"  : [
            "id",
            "managerID",
            "referenceID",
            "referenceName",
            "referenceString",
            "referenceType",
            "uri"
        ],
        "objectsToFlatten" : [
            "source",
            "category",
            "customer"
        ]
    };

    return config;
};