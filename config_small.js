/**
 * Created by hart on 11/7/16.
 */
"use strict";

exports.getConfig = function getConfig() {

    let config = {
        "debug"            : false,
        "debugKeys"        : false,
        "showCount"        : false,
        "setHeaderType"    : true,//TODO set to true
        "printEventHeader" : true,
        "printAttachHeader": true,
        "restURL"           : "https://sol-saturn:8443/www/manager-service/rest",
        "coreURL"           : "https://sol-saturn:8443/www/core-service/rest",
        "baseURL"           : "https://sol-saturn:8443",
        /*
        "restURL"           : "https://va-css-arcesm03.intelgd.com:8443/www/manager-service/rest",
        "coreURL"           : "https://va-css-arcesm03.intelgd.com:8443/www/core-service/rest",
        "baseURL"           : "https://va-css-arcesm03.intelgd.com:8443",
         "arcUser"          : "as-edge-svc-acct",
         "arcPass"          : "rW5gaO9$S[._gdCyber",
        */
        "arcUser"          : "edgeCore",
        "arcPass"          : "corvidae",

    };

    return config;
};