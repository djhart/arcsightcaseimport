/**
 * Created by hart on 11/17/16.
 */
"use strict";
let loki = require('lokijs');

    var db = new loki('db_name.json', {
        persistenceMethod:'fs',
    autosave: true,
    autosaveInterval: 60 * 1000, //every 60 seconds
    autoload: true,
        verbose: true,
        autoloadCallback: init

    });



//var children = db.addCollection('caseDetails')

//console.log("db", db);
function init(){

    let addCollectionOptions = {
        unique: ['name'],
        ttlInterval: 1200,
        autoupdate: true
    };

    let donald = db.getCollection("caseDetails");
    if ( !donald ){
        donald = db.addCollection("caseDetails", addCollectionOptions);
        //donald = db.addCollection("DONALDJHART");

        donald.insert({name:"ruth", age:43, hasAttachments:false, hasEvents: true});
        donald.insert({name:"aesop", age:5, hasEvents: false});
        donald.insert({name:"roku", age:4, hasEvents: true});
        donald.insert({name:"djhart", age:45, hasEvents: true});

        db.saveDatabase(donsEnd);
    }


    let ruth = db.getCollection("attachmentIds");
    if ( !ruth ){
        ruth = db.addCollection("attachmentIds", addCollectionOptions);
        //donald = db.addCollection("DONALDJHART");

        ruth.insert({name:"ruth", age:43, hasAttachments:false, hasEvents: true});
        ruth.insert({name:"aesop", age:5, hasEvents: false});
        ruth.insert({name:"roku", age:4, hasEvents: true});
        ruth.insert({name:"djhart", age:45, hasEvents: true});

        db.saveDatabase(donsEnd);
    }


//console.log(children.get(1));

  // db.saveDatabase(donsEnd);
    db.saveDatabase();

}


function donsEnd(){
    process.exit(0);
}
