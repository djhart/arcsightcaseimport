"use strict";
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

const _ = require('lodash');
const moment = require('moment');
const jp = require('jsonpath');
const request = require('request');
let GLOBAL = require('./configGD').getConfig();


let mode = 'not_set';
const debug = true;
let printEventHeader = true;

const conf = {
    arc_url: 'https://sol-saturn:8443',
    arcUser: 'admin',
    arcPass: 'arcsight'
};

//7kjutB1gBABCADL8wgcwEaA==
//7HCBd7VYBABCDhhxyPYBMog==

function init() {
    getToken(GLOBAL.arcUser, GLOBAL.arcPass, mode, myCallback);
}

function getReportDetails(idList, authToken, caseId) {

    _.each(idList, function (id) {

        let myJson = {
            "rep.getResourceById": {
                "rep.authToken" : authToken,
                "rep.id": encodeURI(id),

            }
        };

        let requestObject = {

            url    : GLOBAL.restURL + '/ReportService/findByUUID',
            method : 'POST',
            headers: [{name: 'Content-Type', value: 'application/json'}],
            json   : myJson
        };

        //console.log("URL:", requestObject.url);

        request.post(requestObject, function (error, response, body) {
           // console.log("\nin attachments:", body);

            if (!error && response.statusCode == 200) {
                const myBody = body["rep.findByUUIDResponse"]["rep.return"];
                console.log("myBody:", myBody);


            }
        });
    });
}

function getReportIds(authToken, callback) {

    let options = {
        url : GLOBAL.restURL + '/ReportService/findAllIds?authToken=' + encodeURIComponent(authToken) + '&alt=json',
        json: true
    };

    //console.log("URL:", options.url);

    request.get(options, (error, response, body) => {
        //console.log("body:", body);
        if (!error && response.statusCode == 200) {
            const idList = body["rep.findAllIdsResponse"]["rep.return"];

            callback(idList, authToken);
        }
    });
}




let myCallback = function (data, mode) {
    getReportIds(data, donCall);
};

let donCall = function (myIds, authToken) {
   getReportDetails(myIds, authToken);

};



let getToken = function (user, pass, mode, callback) {
    let options = {
        url : GLOBAL.coreURL + '/LoginService/login?login=' + encodeURI(user) + '&password=' + encodeURI(pass),
        json: true
    };

    console.log("URL:", options.url);

    request.get(options, function (error, response, body) {
        //console.log("body:", body);
        if (!error && response.statusCode == 200) {
            callback(jp.query(body, '$..*')[1], mode);

        }
    });
};




init();
