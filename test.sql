SELECT Name,
       LOCATE('-', Name),
       CAST(LOCATE('-', Name) AS INT),
       LOCATE('-', Name, 14),
       CAST(LOCATE('-', Name, 14) AS INT),
       SUBSTRING(
       Name, CAST(LOCATE('-',
       Name) AS INT),
       CAST(LOCATE('-', Name, 14) AS INT) - CAST(LOCATE('-', Name) AS INT) )
       FROM src.CasesFromReports